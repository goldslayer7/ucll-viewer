﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WpfViewer
{
    public class Animation
    {
        private readonly IList<WriteableBitmap> frames;

        public Animation(IList<WriteableBitmap> frames)
        {
            this.frames = frames;
        }

        public int FrameCount
        {
            get
            {
                return frames.Count;
            }
        }

        public int FrameWidth
        {
            get
            {
                return frames[0].PixelWidth;
            }
        }

        public int FrameHeight
        {
            get
            {
                return frames[0].PixelHeight;
            }
        }

        public WriteableBitmap GetFrame(int index)
        {
            return frames[index];
        }
    }
}
