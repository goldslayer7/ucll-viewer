﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfViewer
{
    public class AnimationReader
    {
        public Animation Read( string path )
        {
            using ( var mapping = MemoryMappedFile.CreateFromFile( path, System.IO.FileMode.Open, "WpfViewerFileMap", 0, MemoryMappedFileAccess.Read ) )
            {
                using ( var accessor = mapping.CreateViewAccessor( 0, 0, MemoryMappedFileAccess.Read ) )
                {
                    return new Animation( ReadFrames( accessor ) );
                }
            }
        }

        private unsafe List<WriteableBitmap> ReadFrames( MemoryMappedViewAccessor accessor )
        {
            const int HEADER_SIZE = sizeof( uint ) * 3;
            const int COLOR_SIZE = sizeof( double ) * 3;
            byte* buffer = null;

            accessor.SafeMemoryMappedViewHandle.AcquirePointer( ref buffer );

            try
            {
                int frameCount, frameWidth, frameHeight;
                ReadHeader( buffer, out frameCount, out frameWidth, out frameHeight );

                var frameSize = frameWidth * frameHeight * COLOR_SIZE;

                var list = new List<WriteableBitmap>();

                for ( var i = 0; i != frameCount; ++i )
                {
                    byte* pFrame = buffer + HEADER_SIZE + i * frameSize;
                    list.Add( ReadFrame( pFrame, frameWidth, frameHeight ) );
                }

                return list;
            }
            finally
            {
                accessor.SafeMemoryMappedViewHandle.ReleasePointer();
            }
        }

        private unsafe void ReadHeader( byte* byteBuffer, out int frameCount, out int frameWidth, out int frameHeight )
        {
            uint* uintView = (uint*) byteBuffer;

            frameCount = (int) uintView[0];
            frameWidth = (int) uintView[1];
            frameHeight = (int) uintView[2];
        }

        private unsafe WriteableBitmap ReadFrame( byte* byteBuffer, int frameWidth, int frameHeight )
        {
            var pColor = (Color*) byteBuffer;
            var bitmap = new WriteableBitmap( frameWidth, frameHeight, 96, 96, PixelFormats.Pbgra32, null );

            bitmap.Lock();

            try
            {
                int bitmapBuffer = (int) bitmap.BackBuffer;

                for ( var y = 0; y != frameHeight; ++y )
                {
                    int* pInt = (int*) ( ( (int) bitmap.BackBuffer ) + y * bitmap.BackBufferStride );

                    for ( var x = 0; x != frameWidth; ++x )
                    {
                        *pInt = (int) *pColor;

                        ++pInt;
                        ++pColor;
                    }
                }
            }
            finally
            {
                bitmap.AddDirtyRect( new System.Windows.Int32Rect( 0, 0, frameWidth, frameHeight ) );
                bitmap.Unlock();
            }

            return bitmap;
        }

        private struct Color
        {
            public double R, G, B;

            public Color( double r, double g, double b )
            {
                R = r;
                G = g;
                B = b;
            }

            public static explicit operator int( Color color )
            {
                var r = ( (int) (color.R * 255) ) & 0xFF;
                var g = ( (int) (color.G * 255) ) & 0xFF;
                var b = ( (int) (color.B * 255) ) & 0xFF;

                return ( 0xFF << 24 ) | ( r << 16 ) | ( g << 8 ) | b;
            }
        }
    }
}
