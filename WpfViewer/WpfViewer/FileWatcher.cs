﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfViewer
{
    public class FileWatcher
    {
        private readonly FileSystemWatcher watcher;

        private string filePath;

        public FileWatcher()
        {
            this.watcher = new FileSystemWatcher();
            this.watcher.Changed += OnChanged;            
        }

        public string File
        {
            get
            {
                return filePath;
            }
            set
            {
                filePath = value;                

                var containingDirectory = Path.GetDirectoryName( filePath );
                watcher.Path = containingDirectory;
                this.watcher.EnableRaisingEvents = true;
            }
        }

        private void OnChanged( object sender, FileSystemEventArgs args )
        {
            if ( args.FullPath == filePath )
            {
                if ( FileChanged != null )
                {
                    FileChanged( filePath );
                }
            }
        }

        public event Action<string> FileChanged;
    }
}
