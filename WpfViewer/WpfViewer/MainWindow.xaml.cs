﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;

namespace WpfViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly FileWatcher fileWatcher;

        private readonly ViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();

            fileWatcher = new FileWatcher();

            this.DataContext = viewModel = new ViewModel( new Services( this ) );

            var timer = new DispatcherTimer( TimeSpan.FromMilliseconds( 40 ), DispatcherPriority.Background, ( sender, e ) => viewModel.Tick(), this.Dispatcher );
            timer.Start();
        }

        private void OnFileChanged( string path )
        {
            viewModel.PerformOpen( path );
        }

        private class Services : IServices
        {
            private readonly MainWindow parent;

            public Services( MainWindow parent )
            {
                this.parent = parent;
            }

            public string OpenFileDialog()
            {
                var openFileDialog = new OpenFileDialog() { Filter = "Raytracer files|*.raw", Multiselect = false };
                var result = openFileDialog.ShowDialog();

                if ( result.HasValue && result.Value )
                {
                    var filename = openFileDialog.FileName;

                    parent.fileWatcher.File = filename;

                    return filename;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
