﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WpfViewer.Cells;

namespace WpfViewer
{
    public interface IServices
    {
        string OpenFileDialog();
    }

    public class ViewModel
    {
        private readonly IServices services;
        private readonly ICell<Animation> animation;
        private readonly ICell<int> currentFrameIndex;
        private readonly ICell<int> maxFrameIndex;
        private readonly ICell<WriteableBitmap> currentFrame;
        private readonly ICell<bool> animating;
        private readonly ICommand openCommand;
        private readonly ICommand toggleAnimation;
        private readonly ICell<bool> resize;

        public ViewModel( IServices services )
        {
            this.services = services;

            animation = Cell.Create( CreateDummyAnimation() );
            currentFrameIndex = Cell.Create( 0 );
            maxFrameIndex = Cell.Derived( animation, fs => fs.FrameCount - 1 );
            currentFrame = Cell.Derived( animation, currentFrameIndex, ( a, i ) => a.GetFrame( i ) );
            animating = Cell.Create( true );
            resize = Cell.Create( false );

            openCommand = new OpenCommand( this );
            toggleAnimation = new ToggleAnimationCommand( this );
        }

        private static Animation CreateDummyAnimation()
        {
            return new Animation( new List<WriteableBitmap> { CreateDummyBitmap() } );
        }

        private static WriteableBitmap CreateDummyBitmap()
        {
            return new WriteableBitmap( 1, 1, 96, 96, PixelFormats.Pbgra32, null );
        }

        public ICell<WriteableBitmap> CurrentFrame
        {
            get
            {
                return currentFrame;
            }
        }

        public ICell<int> MaxFrameIndex
        {
            get
            {
                return maxFrameIndex;
            }
        }

        public ICell<int> CurrentFrameIndex
        {
            get
            {
                return currentFrameIndex;
            }
        }

        public ICommand Open
        {
            get
            {
                return this.openCommand;
            }
        }

        public ICell<bool> Animating
        {
            get
            {
                return animating;
            }
        }

        private void PerformToggleAnimation()
        {
            this.animating.Value = !this.animating.Value;
        }

        public ICommand ToggleAnimation
        {
            get
            {
                return toggleAnimation;
            }
        }        

        public void Tick()
        {
            if ( animating.Value )
            {
                currentFrameIndex.Value = ( currentFrameIndex.Value + 1 ) % animation.Value.FrameCount;
            }
        }

        public ICell<bool> Resize
        {
            get
            {
                return resize;
            }
        }

        private void PerformOpen()
        {
            var file = services.OpenFileDialog();

            if ( file != null )
            {
                PerformOpen( file );
            }
        }

        public void PerformOpen( string path )
        {
            var animationReader = new AnimationReader();

            animation.Value = animationReader.Read( path );
            currentFrameIndex.Value = 0;
        }

        private abstract class Command : ICommand
        {
            protected readonly ViewModel parent;

            protected Command( ViewModel parent )
            {
                this.parent = parent;
            }

            public bool CanExecute( object parameter )
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            public abstract void Execute( object parameter );
        }

        private class OpenCommand : Command
        {
            public OpenCommand( ViewModel parent )
                : base( parent )
            {
                // NOP
            }

            public override void Execute( object parameter )
            {
                parent.PerformOpen();
            }
        }

        private class ToggleAnimationCommand : Command
        {
            public ToggleAnimationCommand( ViewModel parent )
                : base( parent )
            {
                // NOP
            }

            public override void Execute( object parameter )
            {
                parent.PerformToggleAnimation();
            }
        }
    }
}
