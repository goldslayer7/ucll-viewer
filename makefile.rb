require 'find'
require 'fileutils.rb'

def compile
  FileUtils.mkdir_p 'viewer/bin'

  puts 'Compiling...'
  Dir.chdir 'viewer/src' do
    files = []
    Find.find '.' do |file|
      if file.end_with? '.java'
      then files << file
      end
    end

    puts `javac -d ../bin #{files.join ' '}`
  end
  puts 'Done compiling!'
end

def create_jar
  Dir.chdir 'viewer/bin' do
    files = []

    Find.find '.' do |file|
      if file.end_with? '.class' then
        files << file
      end
    end

    puts "Creating jar..."
    puts `jar cfe ../../viewer.jar viewer.Main #{files.join(' ')}`
    puts "Done!"
  end
end

compile
create_jar
