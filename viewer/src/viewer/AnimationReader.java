package viewer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class AnimationReader
{
    private final ByteBuffer bb;

    public AnimationReader( String path ) throws IOException
    {
        this( FileSystems.getDefault().getPath( path ) );
    }

    public AnimationReader( Path path ) throws IOException
    {
        this( Files.readAllBytes( path ) );
    }

    public AnimationReader( byte[] bytes )
    {
        bb = ByteBuffer.wrap( bytes );
        bb.order( ByteOrder.nativeOrder() );
    }

    public List<Image> loadAnimation()
    {
        List<Image> frames = new ArrayList<Image>();
        int nFrames = readFrameCount();
        int frameWidth = readWidth();
        int frameHeight = readHeight();

        System.out.println( String.format( "Reading %d frames", nFrames ) );

        for ( int i = 0; i != nFrames; ++i )
        {
            System.out.println( String.format( "Reading frame %d", (i + 1) ) );
            frames.add( loadImage( frameWidth, frameHeight ) );
        }

        System.out.println( "Done reading" );

        return frames;
    }

    private Image loadImage(int width, int height)
    {
        Image image = new Image( width, height );

        for ( int y = 0; y != height; ++y )
        {
            for ( int x = 0; x != width; ++x )
            {
                image.pixels[x][y] = readColor();
            }
        }

        return image;
    }

    private int readFrameCount()
    {
        return bb.getInt();
    }

    private int readWidth()
    {
        return bb.getInt();
    }

    private int readHeight()
    {
        return bb.getInt();
    }

    private Color readColor()
    {
        double r = bb.getDouble();
        double g = bb.getDouble();
        double b = bb.getDouble();

        return new Color( r, g, b );
    }
}
