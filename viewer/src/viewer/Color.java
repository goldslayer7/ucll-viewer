package viewer;

public class Color
{
    public double r, g, b;

    public Color()
    {
        this( 0, 0, 0 );
    }

    public Color( double r, double g, double b )
    {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public int asInt()
    {
        int r = ((int) (this.r * 255)) & 0xFF;
        int g = ((int) (this.g * 255)) & 0xFF;
        int b = ((int) (this.b * 255)) & 0xFF;

        return 0xFF000000 | (r << 16) | (g << 8) | b;
    }

    public Color clamp()
    {
        double r = Math.min( 1, this.r );
        double g = Math.min( 1, this.g );
        double b = Math.min( 1, this.b );

        return new Color( r, g, b );
    }
}
